ScriptName QF_MCQuest01_0100182C Extends Quest hidden

;-- Variables ---------------------------------------

;-- Properties --------------------------------------
ReferenceAlias Property Alias_MCBanditBossREF Auto
ReferenceAlias Property Alias_MCBanditNoteREF Auto
ReferenceAlias Property Alias_MCConfectionaryREF Auto
Book Property Alias_MCNote Auto
ReferenceAlias Property Alias_MCNoteREF Auto

;-- Functions ---------------------------------------

Function Fragment_0()
  ; Empty function
EndFunction

; Skipped compiler generated GetState

; Skipped compiler generated GotoState

Function Fragment_4()
  Self.SetObjectiveCompleted(30, 1 as Bool) ; #DEBUG_LINE_NO:28
EndFunction

Function Fragment_2()
  Self.SetObjectiveCompleted(10, True) ; #DEBUG_LINE_NO:44
  Self.SetObjectiveDisplayed(20, 1 as Bool, False) ; #DEBUG_LINE_NO:45
EndFunction

Function Fragment_1()
  Self.SetObjectiveDisplayed(10, 1 as Bool, False) ; #DEBUG_LINE_NO:53
EndFunction

Function Fragment_3()
  Self.SetObjectiveCompleted(20, True) ; #DEBUG_LINE_NO:61
  Self.SetObjectiveDisplayed(30, 1 as Bool, False) ; #DEBUG_LINE_NO:62
EndFunction
