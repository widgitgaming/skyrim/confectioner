ScriptName MCStaffEffectSCRIPT Extends ActiveMagicEffect

;-- Variables ---------------------------------------
Bool TargetIsImmune = True
Race VictimRace
Int randEffect = 0
Actor victim

;-- Properties --------------------------------------
Float Property ShaderDuration = 0.0 Auto
{ Duration of Effect Shader. }
Bool Property bSetAlphaZero = True Auto
{ The Effect Shader we want. }
Potion Property boiledCremeTreat Auto
ObjectReference Property explosionMarker Auto
Float Property fDelay = 0.75 Auto
{ time to wait before Spawning Ash Pile }
Float Property fDelayEnd = 1.649999976 Auto
{ time to wait before Removing Base Actor }
Explosion Property fireSpell Auto
Explosion Property frostSpell Auto
Potion Property honey Auto
Potion Property honeyNutTreat Auto
Keyword Property keyDragon Auto
Keyword Property keyGiant Auto
Keyword Property keyWabbajackExcluded Auto
Potion Property longTaffyTreat Auto
ActorBase Property madWoman Auto
Activator Property pDefaultAshPileGhost Auto
{ The object we use as a pile. }
FormList Property pDisintegrationMainImmunityList Auto
{ If the target is in this list, they will not be disintegrated. }
EffectShader Property pGhostDeathFXShader Auto
{ the shader to play while dying }
Potion Property pie Auto
MiscObject Property plate Auto
Explosion Property stormSpell Auto
Potion Property sweetRoll Auto
Explosion Property visualExplosion Auto

;-- Functions ---------------------------------------

; Skipped compiler generated GetState

; Skipped compiler generated GotoState

Event onEffectStart(Actor akTarget, Actor akCaster)
  victim = akTarget ; #DEBUG_LINE_NO:59
  If akCaster == Game.getPlayer() ; #DEBUG_LINE_NO:61
    randEffect = Utility.randomInt(1, 100) ; #DEBUG_LINE_NO:63
    explosionMarker.moveTo(akTarget as ObjectReference, 0.0, 0.0, 0.0, True) ; #DEBUG_LINE_NO:64
    If akTarget.isEssential() ; #DEBUG_LINE_NO:66
      randEffect = 1 ; #DEBUG_LINE_NO:67
    ElseIf akTarget.hasKeyword(keyWabbajackExcluded) ; #DEBUG_LINE_NO:68
      randEffect = 1 ; #DEBUG_LINE_NO:69
    ElseIf akTarget.hasKeyword(keyGiant) ; #DEBUG_LINE_NO:70
      randEffect = 1 ; #DEBUG_LINE_NO:71
    ElseIf akTarget.hasKeyword(keyDragon) ; #DEBUG_LINE_NO:72
      randEffect = 1 ; #DEBUG_LINE_NO:73
    ElseIf akTarget.isCommandedActor() ; #DEBUG_LINE_NO:74
      randEffect = 1 ; #DEBUG_LINE_NO:75
    ElseIf akTarget.getBaseObject() == madWoman as Form ; #DEBUG_LINE_NO:76
      randEffect = 1 ; #DEBUG_LINE_NO:77
    EndIf
    If randEffect < 11 ; #DEBUG_LINE_NO:82
      Self.elementalEffect(akTarget) ; #DEBUG_LINE_NO:83
    ElseIf randEffect >= 11 && randEffect < 28 ; #DEBUG_LINE_NO:85
      Self.spawnApplePie(akTarget) ; #DEBUG_LINE_NO:86
    ElseIf randEffect >= 28 && randEffect < 45 ; #DEBUG_LINE_NO:88
      Self.spawnCremeTreat(akTarget) ; #DEBUG_LINE_NO:89
    ElseIf randEffect >= 45 && randEffect < 62 ; #DEBUG_LINE_NO:91
      Self.spawnHoney(akTarget) ; #DEBUG_LINE_NO:92
    ElseIf randEffect >= 62 && randEffect < 79 ; #DEBUG_LINE_NO:94
      Self.spawnHoneyNutTreat(akTarget) ; #DEBUG_LINE_NO:95
    ElseIf randEffect >= 79 && randEffect < 96 ; #DEBUG_LINE_NO:97
      Self.spawnTaffyTreat(akTarget) ; #DEBUG_LINE_NO:98
    ElseIf randEffect >= 96 ; #DEBUG_LINE_NO:100
      Self.spawnSweetRoll(akTarget) ; #DEBUG_LINE_NO:101
    EndIf
  EndIf
EndEvent

Function elementalEffect(Actor targ)
  Int rand = Utility.randomInt(1, 3) ; #DEBUG_LINE_NO:110
  explosionMarker.setPosition(targ.x, targ.y, targ.z + 75 as Float) ; #DEBUG_LINE_NO:111
  If rand == 1 ; #DEBUG_LINE_NO:113
    targ.placeAtMe(fireSpell as Form, 1, False, False) ; #DEBUG_LINE_NO:114
    targ.damageAV("Health", 60 as Float) ; #DEBUG_LINE_NO:115
  ElseIf rand == 2 ; #DEBUG_LINE_NO:117
    targ.placeAtMe(frostSpell as Form, 1, False, False) ; #DEBUG_LINE_NO:118
    targ.damageAV("Health", 60 as Float) ; #DEBUG_LINE_NO:119
  ElseIf rand == 3 ; #DEBUG_LINE_NO:121
    targ.placeAtMe(stormSpell as Form, 1, False, False) ; #DEBUG_LINE_NO:122
    targ.damageAV("Health", 60 as Float) ; #DEBUG_LINE_NO:123
  EndIf
EndFunction

Function spawnApplePie(Actor targ)
  targ.placeAtMe(visualExplosion as Form, 1, False, False) ; #DEBUG_LINE_NO:130
  Utility.wait(0.699999988) ; #DEBUG_LINE_NO:132
  explosionMarker.setPosition(targ.x, targ.y, targ.z + 10 as Float) ; #DEBUG_LINE_NO:133
  Utility.wait(0.100000001) ; #DEBUG_LINE_NO:134
  Self.createAshPile() ; #DEBUG_LINE_NO:135
  explosionMarker.placeAtMe(pie as Form, 1, False, False) ; #DEBUG_LINE_NO:136
EndFunction

Function spawnCremeTreat(Actor targ)
  targ.placeAtMe(visualExplosion as Form, 1, False, False) ; #DEBUG_LINE_NO:141
  Utility.wait(0.699999988) ; #DEBUG_LINE_NO:143
  explosionMarker.setPosition(targ.x, targ.y, targ.z + 10 as Float) ; #DEBUG_LINE_NO:144
  Utility.wait(0.100000001) ; #DEBUG_LINE_NO:145
  Self.createAshPile() ; #DEBUG_LINE_NO:146
  explosionMarker.placeAtMe(boiledCremeTreat as Form, 1, False, False) ; #DEBUG_LINE_NO:147
EndFunction

Function spawnHoney(Actor targ)
  targ.placeAtMe(visualExplosion as Form, 1, False, False) ; #DEBUG_LINE_NO:152
  Utility.wait(0.699999988) ; #DEBUG_LINE_NO:154
  explosionMarker.setPosition(targ.x, targ.y, targ.z + 10 as Float) ; #DEBUG_LINE_NO:155
  Utility.wait(0.100000001) ; #DEBUG_LINE_NO:156
  Self.createAshPile() ; #DEBUG_LINE_NO:157
  explosionMarker.placeAtMe(honey as Form, 1, False, False) ; #DEBUG_LINE_NO:158
EndFunction

Function spawnHoneyNutTreat(Actor targ)
  targ.placeAtMe(visualExplosion as Form, 1, False, False) ; #DEBUG_LINE_NO:163
  Utility.wait(0.699999988) ; #DEBUG_LINE_NO:165
  explosionMarker.setPosition(targ.x, targ.y, targ.z + 10 as Float) ; #DEBUG_LINE_NO:166
  Utility.wait(0.100000001) ; #DEBUG_LINE_NO:167
  Self.createAshPile() ; #DEBUG_LINE_NO:168
  explosionMarker.placeAtMe(honeyNutTreat as Form, 1, False, False) ; #DEBUG_LINE_NO:169
EndFunction

Function spawnTaffyTreat(Actor targ)
  targ.placeAtMe(visualExplosion as Form, 1, False, False) ; #DEBUG_LINE_NO:174
  Utility.wait(0.699999988) ; #DEBUG_LINE_NO:176
  explosionMarker.setPosition(targ.x, targ.y, targ.z + 10 as Float) ; #DEBUG_LINE_NO:177
  Utility.wait(0.100000001) ; #DEBUG_LINE_NO:178
  Self.createAshPile() ; #DEBUG_LINE_NO:179
  explosionMarker.placeAtMe(longTaffyTreat as Form, 1, False, False) ; #DEBUG_LINE_NO:180
EndFunction

Function spawnSweetRoll(Actor targ)
  targ.placeAtMe(visualExplosion as Form, 1, False, False) ; #DEBUG_LINE_NO:185
  Utility.wait(0.699999988) ; #DEBUG_LINE_NO:187
  explosionMarker.setPosition(targ.x, targ.y, targ.z + 10 as Float) ; #DEBUG_LINE_NO:188
  Utility.wait(0.100000001) ; #DEBUG_LINE_NO:189
  Self.createAshPile() ; #DEBUG_LINE_NO:190
  explosionMarker.placeAtMe(sweetRoll as Form, 1, False, False) ; #DEBUG_LINE_NO:191
EndFunction

Function createAshPile()
  If pDisintegrationMainImmunityList == None ; #DEBUG_LINE_NO:197
    TargetIsImmune = False ; #DEBUG_LINE_NO:198
  Else
    ActorBase VictimBase = victim.getBaseObject() as ActorBase ; #DEBUG_LINE_NO:200
    VictimRace = VictimBase.GetRace() ; #DEBUG_LINE_NO:201
    If pDisintegrationMainImmunityList.hasform(VictimRace as Form) || pDisintegrationMainImmunityList.hasform(VictimBase as Form) ; #DEBUG_LINE_NO:203
      TargetIsImmune = True ; #DEBUG_LINE_NO:204
    Else
      TargetIsImmune = False ; #DEBUG_LINE_NO:206
    EndIf
  EndIf
  If TargetIsImmune == False ; #DEBUG_LINE_NO:211
    victim.kill(Game.getPlayer()) ; #DEBUG_LINE_NO:215
    victim.SetCriticalStage(victim.CritStage_DisintegrateStart) ; #DEBUG_LINE_NO:216
    If pGhostDeathFXShader != None ; #DEBUG_LINE_NO:218
      pGhostDeathFXShader.play(victim as ObjectReference, ShaderDuration) ; #DEBUG_LINE_NO:219
    EndIf
    victim.SetAlpha(0.0, True) ; #DEBUG_LINE_NO:222
    victim.AttachAshPile(pDefaultAshPileGhost as Form) ; #DEBUG_LINE_NO:225
    Utility.wait(fDelayEnd) ; #DEBUG_LINE_NO:227
    If pGhostDeathFXShader != None ; #DEBUG_LINE_NO:228
      pGhostDeathFXShader.stop(victim as ObjectReference) ; #DEBUG_LINE_NO:229
    EndIf
    If bSetAlphaZero == True ; #DEBUG_LINE_NO:231
      victim.SetAlpha(0.0, True) ; #DEBUG_LINE_NO:232
    EndIf
    victim.SetCriticalStage(victim.CritStage_DisintegrateEnd) ; #DEBUG_LINE_NO:234
  EndIf
EndFunction
