# Confectioner

Follow the clues to find the lost Staff of the Confectioner and claim it for
your own!

## Description

Many years ago, there was a mysterious man known as the Confectioner. Over the
years, he created the most exquisite sweets in all of Tamriel until, one day,
he simply vanished without a trace. With no record of his true identity, his
secrets seemed to have been taken to his grave... or have they?

Follow the clues to find the lost Staff of the Confectioner and claim it for
your own!

## Compatibility

To my knowledge, Confectioner has no conflicts. If you do find a conflict,
please let me know!

## Bugs

If you find an issue, let us know [here](https://gitlab.com/widgitgaming/skyrim/confectioner)!
